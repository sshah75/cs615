#!/bin/bash

# ~ not expanding fix
EC2_BACKUP_FLAGS_SSH=$(eval "echo $EC2_BACKUP_FLAGS_SSH")

#global vars
region="us-east-1"
availability_zone=
declare -A ami_ids

#ubuntu AMIs
ami_ids[ap-northeast-1]=ami-8d6d9d8d
ami_ids[ap-southeast-1]=ami-e8f1c1ba
ami_ids[ap-southeast-2]=ami-7163104b
ami_ids[eu-central-1]=ami-b6cff2ab
ami_ids[eu-west-1]=ami-5da23a2a
ami_ids[sa-east-1]=ami-55883348
ami_ids[us-east-1]=ami-d85e75b0
ami_ids[us-west-1]=ami-d16a8b95
ami_ids[us-west-2]=ami-6989a659
ami_user=ubuntu
backup_device=/dev/xvdx
backup_partition=/dev/xvdx1

max_timeout_tries=15

# print debug statement if environment variable set
debug()
{
    if [ "$EC2_BACKUP_VERBOSE" != "" ]; then
        echo $*
    fi
}

# terminate instance if it has been created
# delete volume by default, pass false to not delete
# param: deleteVolume (true)
cleanup()
{
    if [ "$instance_id" != "" ]; then
        debug "Terminating instance $instance_id."
        ec2-terminate-instances --region $region $instance_id > /dev/null
        # TODO: perhaps check if above command returns non-zero
    fi
    # delete volume by default
    if [[ "$1" != "false" && "$new_volume_created" != "" && "$volume_id" != "" ]]; then
        debug "Detaching and deleting volume $volume_id"
        ec2-detach-volume --region $region $volume_id --force
        while [ $(ec2-describe-volumes --region $region | grep $volume_id | head -1 | awk '{print $5}') != "available" ]
        do
            debug "Waiting for volume to become available after detachment."
        done
        ec2-delete-volume --region $region $volume_id
    fi
}

check_for_failure()
{
    if [ $? -ne 0 ]; then
        echo $1 1>&2
        cleanup
        exit 1
    fi
}

wait_for_ssh()
{
    debug "Waiting for AWS instance to allow SSH connection."
    #http://serverfault.com/questions/388184/waiting-until-tcp-socket-is-available-in-bash
    local tries=0
    nc -z -w 10 $instance_host 22   #22 is ssh port
    while [[ $? -ne 0 && $tries -ne $max_timeout_tries ]]
    do
        ((tries++))
        debug "$tries: Trying to connect to ssh"
        sleep 10
        nc -z -w 10 $instance_host 22
    done

    if [ $tries -eq $max_timeout_tries ]; then
        cleanup
        echo "Unable to ssh to AWS instance." 1>&2
        exit 1
    fi
}

print_help()
{
    echo "usage:" $0 "[-h] [-m method] [-v volume-id] dir"
}

find_dir()
{
    for arg in "$@"
    do
        if [[ $arg =~ ^- ]]; then
            #current arg is a flag
            local prev_flag=$arg
        else
            if [ "$prev_flag" == "" ]; then
                #current arg is not a flag and prev arg wasn't a flag
                #this must be the dir
                dir=$arg
                break
            else
                #current arg is not a flag but prev arg was a flag
                #this arg is arg to prev flag so unset prev_flag
                local prev_flag=
            fi
        fi
    done
}

method=$(echo $* | grep -P -o '(?<=-m )[^\s]+')
volume_id=$(echo $* | grep -P -o '(?<=-v )[^\s]+')
help=$(echo $* | grep -o '\-h')
dir=

find_dir "$@"

if [ "$help" = "-h" ]; then
    print_help
    exit 0
fi

if [ "$method" = "" ]; then
    method="dd"
fi

if [[ "$method" != "dd"  && "$method" != "rsync" ]]; then
    echo "Invalid method specified. Valid methods are 'dd' and 'rsync'."
    exit 1
fi

if [ "$dir" = "" ]; then
    echo "No directory specified." 1>&2
    exit 1
fi

# check if directory exists
if [ ! -d "$dir" ]; then
    echo "Specified directory does not exist." 1>&2
    exit 1
fi

debug "method:" $method
debug "volume-id:" $volume_id
debug "dir:" $dir

#---------program starts--------------

debug "Getting list of available regions."
available_regions=( $(ec2-describe-regions | awk '{print $2}') )
if [[ $? -ne 0 || ${#available_regions[@]} -eq 0 ]]; then
    cleanup
    echo "No available regions. Please ensure you have set up your environment variables for AWS." 1>&2
    exit 1
fi

debug "Available regions:" ${available_regions[@]}

debug "Getting first availability zone"
availability_zone=$(ec2-describe-availability-zones --region $region | head -1 | awk '{print $2}')``
debug "Using availability zone: $availability_zone"

# size of the directory we will be backing up
debug "Calculating size of specified directory."
dir_size=$(du -c -BG $dir | grep total | awk '{print $1}' | grep -o ".*[^G]")
vol_size=$(($dir_size * 2))
debug "Directory size is $dir_size"
debug "Volume size will be $vol_size"

# create the new volume
if [ "$volume_id" = "" ]; then
    debug "Creating new volume on AWS."
    output=$(ec2-create-volume -s $vol_size -z $availability_zone)
    check_for_failure "Unable to create volume on AWS."
    volume_id=$(echo $output | awk '{print $2}')
    new_volume_created=true
    debug "Created new volume $volume_id"
else
    # check if volume_id actually exists
    debug "Checking if specified volume-id $volume_id exists on AWS."
    for r in "${available_regions[@]}"
    do
        debug "Checking region $r."
        if [[ $(ec2-describe-volumes --region $r | grep -o $volume_id) == $volume_id ]]; then
            debug "Volume $volume_id in region $r"
            region=$r
            break
        fi
    done
fi

debug "Getting availability zone from region."
availability_zone=$(ec2-describe-availability-zones --region $region | head -1 | awk '{print $2}')
debug "Availability zone: $availability_zone"

if [ "$availability_zone" == "" ]; then
    echo "No zone available for region $region." 1>&2
    cleanup
    exit 1
fi

# create new instance
debug "Creating new instance ${ami_ids[$region]} in region $region on AWS."
output=$(ec2-run-instances ${ami_ids[$region]} $EC2_BACKUP_FLAGS_AWS --instance-type t1.micro --region $region -z $availability_zone)
check_for_failure "Unable to create instance on AWS. This program creates an AWS instance "\
"in us-east-1 by default if no volume is specified. Please ensure your environment "\
"variables have security groups and key pairs that are in the us-east-1 region."

# storing the output of ec2-run-instances in a variable removes the newlines
# so we need grep -o "INSTANCE.*" to get the (previously) second line
instance_id=$(echo $output | grep -o "INSTANCE.*" | awk '{print $2}')
debug "Created new instance $instance_id."

# wait until volume is ready
while [ $(ec2-describe-volumes --region $region | grep $volume_id | awk '{print $5}') != "available" ]
do
    debug "Waiting for volume to become available."
done


# wait until instance is ready
while [ $(ec2-describe-instances --region $region | grep $instance_id | awk '{print $6}') != "running" ]
do
    debug "Waiting for instance to become available."
done

# get instance host
instance_host=$(ec2-describe-instances --region $region | grep $instance_id | awk '{print $4}')
debug "instance_host: $instance_host"

#-----------at this point both the instance and volume are ready----------------

# attach the volume to the instance
debug "Attaching volume $volume_id to instance $instance_id on $backup_device."
ec2-attach-volume $volume_id --region $region --i $instance_id --device $backup_device > /dev/null
check_for_failure "Unable to attach volume to instance."

wait_for_ssh

# check if we can ssh
# debug "Checking if can ssh into AWS instance."
# ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host 'uname' > /dev/null
# if [ "$?" != 0 ]; then
#     cleanup
#     echo "Unable to ssh into instance. Please ensure your EC2_BACKUP_FLAGS_SSH variable is "\
#     "configured correctly. i.e. export EC2_BACKUP_FLAGS_SSH=/home/$USER/.ssh/aws.pem"
#     exit 1
# fi

ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "echo"
check_for_failure "Failed to ssh into AWS instance. Please check your EC2_BACKUP_FLAGS_SSH environment variable."

#ssh into instance and perform backup
if [ $method == "dd" ]; then
    debug "Backing up using dd method."
    # tar + dd
    # tar -cz $dir | ssh -o StrictHostKeyChecking=no $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "tar xzO | dd of=$backup_device"
    tar -cf - $dir | ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "sudo dd of=$backup_device bs=64k conv=block"
    check_for_failure "Failed to backup."
fi

if [ $method == "rsync" ]; then
    debug "Backing up using rsync method"
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "sudo mkdir -p /mnt/ec2-backup"
    check_for_failure "Failed to create mount directory on AWS instance."
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "echo -e 'n\np\n1\n\n\nw\n' | sudo fdisk $backup_device"
    check_for_failure "Failed to create partition on AWS volume."
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "sudo mkfs -t ext4 $backup_partition"
    check_for_failure "Failed to create filesystem on AWS volume partition."
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH $ami_user@$instance_host "sudo mount $backup_partition /mnt/ec2-backup && sudo chown -R $ami_user /mnt/ec2-backup"
    check_for_failure "Failed to mount AWS volume partition and change permissions of mount directory."
    rsync -e "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=quiet $EC2_BACKUP_FLAGS_SSH" -az --rsync-path="sudo rsync" $dir "$ami_user@$instance_host:/mnt/ec2-backup"
    check_for_failure "Failed to backup."
fi

cleanup false   # don't delete volume
echo $volume_id 1>&2

exit 0